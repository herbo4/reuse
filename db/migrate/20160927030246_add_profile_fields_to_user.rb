class AddProfileFieldsToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :name, :string
    add_column :users, :phone, :string
    add_column :users, :intro, :string
    add_column :users, :bio, :string
    add_column :users, :twitter, :string
    add_column :users, :facebook, :string
    add_column :users, :email_notifications, :boolean
    add_column :users, :message_count, :integer
    add_column :users, :role, :string
  end
end
