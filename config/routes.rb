Rails.application.routes.draw do
  devise_for :users
  root 'sink#index'
  get "profile" => "users#show", :as => 'profile'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
